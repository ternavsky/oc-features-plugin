<?php
/**
 * Copyright (c) 2016 Planeta del Este .
 *
 * Icons.php is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PlanetaDelEste\Features\Controllers;

use BackendMenu;
use Backend\Classes\Controller;
use Cache;
use PlanetaDelEste\Features\Models\Icon;
use Redirect;

/**
 * Icons Back-end Controller
 */
class Icons extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['planetadeleste.features.access_icons'];
    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('PlanetaDelEste.Features', 'features', 'icons');
        $this->addCss('/plugins/planetadeleste/features/assets/css/planetadeleste-features-backend.css');
        $this->addJs('/plugins/planetadeleste/features/assets/js/app.js');
    }

    public function update($recordId, $context = null)
    {
        $icon = Icon::find($recordId);
        if($icon->url) {
            $this->addCss( $icon->url );
        }

        $this->addCss('/plugins/planetadeleste/features/assets/vendor/codemirror/lib/codemirror.css');
        $this->addCss('/plugins/planetadeleste/features/assets/vendor/codemirror/addon/fold/foldgutter.css');
        $this->addCss('/plugins/planetadeleste/features/assets/vendor/codemirror/theme/monokai.css');
        $this->addJs('/plugins/planetadeleste/features/assets/vendor/codemirror/lib/codemirror.js');
        $this->addJs('/plugins/planetadeleste/features/assets/vendor/codemirror/addon/fold/foldcode.js');
        $this->addJs('/plugins/planetadeleste/features/assets/vendor/codemirror/addon/fold/foldgutter.js');
        $this->addJs('/plugins/planetadeleste/features/assets/vendor/codemirror/addon/fold/brace-fold.js');
        $this->addJs('/plugins/planetadeleste/features/assets/vendor/codemirror/addon/fold/comment-fold.js');
        $this->addJs('/plugins/planetadeleste/features/assets/vendor/codemirror/mode/css/css.js');

        // Call the FormController behavior update() method
        return $this->asExtension('FormController')->update($recordId, $context);
    }

    public function update_onRemoveIcons($recordId)
    {
        $remove = post('Icon.delete');
        if($remove && $model = Icon::find($recordId)) {
            if(!is_array($remove))
                $remove = [$remove];

            $icons = $model->getIcons();
            $filtered = $icons->filter(function ($item) use ($remove) {
                return (!in_array($item, $remove));
            });
            $model->storeIcons($filtered->all(), $model->path);

            $this->vars['model'] = $model = Icon::find($recordId);
            return ['section#'.$model->slug => $this->makePartial('view_icons')];
        }
    }

    public function update_onUpdateCode($recordId)
    {
        $code = post('Icon.code');
        if($code && $icon = Icon::find($recordId)) {
            if($icon->updateContent($code)){
                return Redirect::refresh();
            }
        }
    }
}