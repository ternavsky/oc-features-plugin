<?php
/**
 * Copyright (c) 2016 Planeta del Este .
 *
 * Categories.php is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PlanetaDelEste\Features\Controllers;

use BackendMenu;
use Backend\Classes\Controller;

/**
 * Categories Back-end Controller
 */
class Categories extends Controller
{
    public $implement = [
        'Backend.Behaviors.FormController',
        'Backend.Behaviors.ListController',
        'PlanetaDelEste.Features.Behaviors.ModalController'
    ];

    public $formConfig = 'config_form.yaml';
    public $listConfig = 'config_list.yaml';

    public $requiredPermissions = ['planetadeleste.features.access_categories'];
    public $bodyClass = 'compact-container';

    public function __construct()
    {
        parent::__construct();

        BackendMenu::setContext('PlanetaDelEste.Features', 'features', 'categories');
    }

    public function onCreateForm()
    {
        $this->asExtension('FormController')->create();
        $this->vars['title'] = trans(
            'pronet.realestate::lang.global.label.create',
            ['name' => trans_choice('pronet.realestate::lang.attributecategories.title', 1)]
        );
        return $this->makePartial('@/plugins/planetadeleste/features/partials/_create_form.htm');
    }

    public function onCreate()
    {
        $this->asExtension('FormController')->create_onSave();
        return $this->listRefresh();
    }

    public function onUpdateForm()
    {
        $this->asExtension('FormController')->update(post('record_id'));
        $this->vars['recordId'] = post('record_id');
        $this->vars['title']    = trans(
            'pronet.realestate::lang.global.label.update',
            ['name' => trans_choice('pronet.realestate::lang.attributecategories.title', 1)]
        );
        return $this->makePartial('@/plugins/planetadeleste/features/partials/_update_form.htm');
    }

    public function onUpdate()
    {
        $this->asExtension('FormController')->update_onSave(post('record_id'));
        return $this->listRefresh();
    }

    public function onDelete()
    {
        $this->asExtension('FormController')->update_onDelete(post('record_id'));
        return $this->listRefresh();
    }
}