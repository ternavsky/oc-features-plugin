<?php
/**
 * Copyright (c) 2016 Planeta del Este .
 *
 * Icon.php is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PlanetaDelEste\Features\Models;

use Cache;
use Chumper\Zipper\Zipper;
use File;
use Lang;
use Model;
use October\Rain\Support\Collection as OctoberCollection;
use Sabberworm\CSS\Parser;
use Symfony\Component\Yaml\Dumper;
use SystemException;
use Yaml;

/**
 * Icon Model
 * @property string                     slug
 * @property array                      icons
 * @property string                     path
 * @property string                     prefix
 * @property string                     library
 * @property \System\Models\File|string package
 * @property mixed|string               url
 * @property string                     basename
 */
class Icon extends Model
{
    use \October\Rain\Database\Traits\Sluggable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'planetadeleste_features_icons';

    /**
     * @var string Storage downloaded files
     */
    public $storage_path = 'app/planetadeleste/features/icon';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = [];

    //protected $jsonable = ['icons'];

    protected $slugs = ['slug' => 'library'];

    public $attachOne = [
        'package' => ['System\Models\File']
    ];

    /**
     * @return int
     */
    public function getCountIconsAttribute()
    {
        return count($this->icons);
    }


    /**
     * @return array
     */
    public function getIconsOptions()
    {
        /*$prefix = $this->prefix;
        $func = function($icon) use ( $prefix ) {
            return  "{i.{$prefix}.{$icon}} {$icon}";
        };
        $labels = array_map($func, $this->icons);*/
        if (!is_array($this->icons)) {
            return [];
        }

        return array_combine($this->icons, $this->icons);
    }

    /**
     * @param \Cms\Classes\Controller|\Backend\Classes\Controller $controller
     */
    public static function loadAssets($controller)
    {
        foreach (Icon::all() as $icon) {
            if ($icon->url) {
                $controller->addCss($icon->url.'?v'.$icon->updated_at->timestamp);
            }
        }
    }

    /**
     * @param string $icon
     *
     * @return string
     */
    public function iconLabel($icon)
    {
        $parts = explode('-', $icon);
        if ($this->prefix) {
            if ($parts[0] == $this->prefix) {
                array_shift($parts);
            }
        }
        $parts = array_map(
            function ($str) {
                return studly_case($str);
            },
            $parts
        );

        return join(' ', $parts);
    }

    public function beforeCreate()
    {
        unset($this->create_method);
    }

    public function beforeSave()
    {
        $url = post('Icon.url');
        if ($url && strstr($url, "http") && !$this->package) {
            $path = storage_path($this->storage_path);
            if (!File::isDirectory($path)) {
                File::makeDirectory($path);
            }
            $filename = $this->slug.'.css';
            $file_css = $path.'/'.$filename;
            $this->downloadFromUrl(post('Icon.url'), $file_css);
            if (File::exists($file_css)) {
                $this->icons = $this->parseCss(File::get($file_css));
                $this->basename = $filename;
                $this->url = '/storage'.$this->storage_path.'/'.basename($file_css);
            }
        } else {
            $sessionKey = post('_session_key');
            /** @var \October\Rain\Database\Attach\File $package */
            $package = $this->package()->withDeferred($sessionKey)->first();
            if (!$this->package && $package) {
                $packageDiskPath = $package->getDiskPath();
                $packagePath = $package->getPath();
                $file_zip = storage_path('app/'.$packageDiskPath);
                $path = storage_path(
                    'app/'.dirname($packageDiskPath).'/'.File::name($packagePath)
                );
                //$url = '/storage/app/'.dirname($packageDiskPath).'/'.File::name($packagePath);
                if (!$this->basename || !File::exists($path.'/'.$this->basename)) {
                    if (!File::exists($path)) {
                        File::makeDirectory($path, 0777, true);
                    }
                    $zipper = new Zipper;
                    $zipper->make($file_zip)->extractTo($path);
                    $files = File::allFiles($path);
                    if (count($files)) {
                        /** @var \SplFileInfo $file */
                        foreach ($files as $file) {
                            if ($file->getExtension() == 'css') {
                                $file_css = $file->getRealPath();
                                $this->basename = $file->getBasename();
                                $this->path = $path;
                                $this->url = str_replace($_SERVER['DOCUMENT_ROOT'], '', $file->getRealPath());
                                $icons = $this->parseCss(File::get($file_css));
                                $this->storeIcons($icons, $path);
                                $this->icons = null;
                                //$this->save();
                                break;
                            }
                        }
                    }
                }
            }
        }
    }


    /**
     * @param array  $icons
     * @param string $path
     *
     * @return int
     * @throws SystemException
     */
    public function storeIcons($icons, $path)
    {
        $icons_file = $path.'/icons.yaml';

        $collection = collect($icons);
        $icons = $collection->filter(function($item) {
            return (boolean)preg_match('/^[a-z0-9-_]+$/', $item);
        });

        $yaml = Yaml::render(['icons' => $icons->values()->all()]);

        if (@File::put($icons_file, $yaml, true) === false) {
            throw new SystemException(Lang::get('planetadeleste.features::lang.common.error_generating_file', ['path'=>$icons_file]));
        }
        Cache::flush();
        @File::chmod($icons_file);
    }

    public function beforeDelete()
    {
        if ($this->path) {
            //$file = $path = storage_path($this->storage_path).'/'.$this->path;
            if (File::exists($this->path)) {
                File::deleteDirectory($this->path);
            }
        }
    }

    public function getContentAttribute()
    {
        $content = '';
        if ($this->url && !strstr($this->url, "http:// ")) {
            $file = base_path(trim($this->url, '/'));
            if (!File::exists($file)) {
                $file = storage_path($this->storage_path).'/'.$this->path;
            }
            if (File::exists($file)) {
                return File::get($file);
            }
        }

        return $content;
    }

    public function getIconsAttribute($value)
    {
        if(!File::exists($this->path.'/icons.yaml'))
            return [];

        $icons = Yaml::parseFile($this->path.'/icons.yaml');

        return array_get($icons, 'icons', []);
    }

    /**
     * @return OctoberCollection
     */
    public function getIcons()
    {
        $icons = $this->icons;
        return new OctoberCollection($icons);
    }


    /**
     * @param $content
     *
     * @return bool|int
     */
    public function updateContent($content)
    {
        if ($this->url && !strstr($this->url, "http:// ")) {
            $file = base_path(trim($this->url, '/'));
            if (!File::exists($file)) {
                $file = storage_path($this->storage_path).'/'.$this->path;
            }
            if (File::exists($file)) {
                return File::put($file, $content);
            }
        }

        return false;
    }

    /**
     * @param string $stylesheet
     *
     * @return array
     */
    private function parseCss($stylesheet)
    {
        if (is_null($stylesheet) || !$stylesheet) {
            return [];
        }

        $css_parse = new Parser($stylesheet);
        $css = $css_parse->parse();
        $icons = [];
        foreach ($css->getAllDeclarationBlocks() as $block) {
            foreach ($block->getSelectors() as $selector) {
                $icons[] = str_replace(['.', ':', 'before', 'after'], '', $selector->getSelector());
            }
        }
        array_unique($icons);
        sort($icons);

        return $icons;
    }

    private function downloadFromUrl($url, $outFileName)
    {
        //file_put_contents($xmlFileName, fopen($link, 'r'));
        //copy($link, $xmlFileName); // download xml file

        if (is_file($url)) {
            copy($url, $outFileName); // download xml file
        } else {
            $options = [
                CURLOPT_FILE    => fopen($outFileName, 'w'),
                CURLOPT_TIMEOUT => 28800, // set this to 8 hours so we dont timeout on big files
                CURLOPT_URL     => $url
            ];

            $ch = curl_init();
            curl_setopt_array($ch, $options);
            curl_exec($ch);
            curl_close($ch);
        }
    }

}