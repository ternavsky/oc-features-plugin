<?php
/**
 * Copyright (c) 2016 Planeta del Este .
 *
 * Feature.php is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PlanetaDelEste\Features\Models;

use Model;

/**
 * Feature Model
 * @property mixed|string icon
 */
class Feature extends Model
{
    use \October\Rain\Database\Traits\Sortable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'planetadeleste_features_features';

    /**
     * @var array Guarded fields
     */
    protected $guarded = [];

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['sort_order', 'name', 'icon', 'is_hidden', 'has_value', 'in_search'];

    /**
     * @var array Relations
     */
    public $belongsTo = [
        'category' => ['PlanetaDelEste\Features\Models\Category']
    ];

    public function scopeVisible($query) {
        return $query->whereIsHidden(false);
    }

    public function scopeHidden($query){
        return $query->whereIsHidden(true);
    }

    public function scopeHasValue($query) {
        return $query->whereHasValue(true);
    }

    public function scopeHasNoValue($query) {
        return $query->whereHasValue(false);
    }

    public function getIconImageAttribute() {
        if($this->icon) {
            return '<i class="' . $this->icon . ' fa-2x"></i>';
        }
        return null;
    }

    public function getInfo()
    {
        return $this->pivot->info ?: $this->info;
    }

}