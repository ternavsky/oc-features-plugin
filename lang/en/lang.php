<?php
/**
 * Copyright (c) 2016 Planeta del Este .
 *
 * lang.php is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * josegarrido.local
 * Created by 1505.
 * User: 1505
 * Date: 18/05/2015
 * Time: 7:17
 */

return [
    'app'         => [
        'name'        => 'Features',
        'description' => 'Create categorized attributes to use with your own plugin. Like Ecommerce, Products listing, Real Estate, or any plugin requiring attributes.',
    ],
    'features'    => [
        'menu_label'    => 'Features',
        'title'         => 'Feature|Features',
        'reorder_title' => 'Reorder features',
    ],
    'categories'  => [
        'menu_label' => 'Categories',
    ],
    'category'    => [
        'create_title' => 'New Category',
    ],
    'icons'       => [
        'create_title' => 'New Icon library',
        'menu_label'   => 'Icons',
    ],
    'fields'      => [
        'name'                => 'Name',
        'url'                 => 'Url',
        'library'             => 'Library Name',
        'icon_prefix'         => 'Icon Prefix',
        'icons'               => 'Icons',
        'icon'                => 'Icon',
        'count_icons'         => 'Icons',
        'extra_css'           => 'Extra Stylesheet',
        'category'            => 'Category',
        'feature'             => 'Feature',
        'in_search'           => 'Show in search form (frontend)',
        'is_hidden'           => 'Hidden',
        'has_value'           => 'Require value',
        'has_no_value'        => 'No value required',
        'package'             => 'Package',
        'sort_order'          => 'Order',
        'overview'            => 'Overview',
        'info'                => 'Information',
        'info_comment'        => 'Extra information, can be override when associate this feature',
        'url_comment'         => 'Write any external css icons package. Must be a secure CDN, like https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css',
        'package_comment'     => 'Upload a zipped package with fonts and css files.',
        'library_comment'     => 'Name your icons library',
        'icon_prefix_comment' => 'Some icons must use prefix class. Like FontAwesome, use "fa"',
        'extra_css_comment'   => 'Can write some extra stylesheet here',
        'create_method'       => 'Select how create icons library',
        'create_by_url'       => 'Create by url',
        'create_by_package'   => 'Create by uploading zip package',
    ],
    'formwidgets' => [
        'iconselector' => [
            'name' => 'Icon Selector',
        ],
    ],
    'tab'         => [
        'library'    => 'Library',
        'icons'      => 'Icons',
        'view_icons' => 'View Icons',
        'editor'     => 'Editor',
    ],
    'permission'  => [
        'access_features'   => 'Manage features',
        'access_categories' => 'Manage categories',
        'access_icons'      => 'Manage icons',
    ],
    'common' => [
        'error_generating_file' => 'Error generating file :path'
    ]
];