<?php
/**
 * Copyright (c) 2016 Planeta del Este .
 *
 * lang.php is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * josegarrido.local
 * Created by 1505.
 * User: 1505
 * Date: 18/05/2015
 * Time: 7:17
 */

return [
    'app'         => [
        'name'        => 'Atributos',
        'description' => 'Cree atributos categorizados apra usar en su propio plugin. Como Carrito, Productos, Inmobiliaria, o cualuqier plugin que necesite de atributos.',
    ],
    'features'    => [
        'menu_label'    => 'Atributos',
        'title'         => 'Atributo|Atributos',
        'reorder_title' => 'Ordenar atributos',
    ],
    'categories'  => [
        'menu_label' => 'Categorías',
    ],
    'category'    => [
        'create_title' => 'Nueva Categoría',
    ],
    'icons'       => [
        'create_title' => 'Nueva librería de íconos',
        'menu_label'   => 'Iconos',
    ],
    'fields'      => [
        'name'                => 'Nombre',
        'url'                 => 'Url',
        'library'             => 'Nombre de la librería',
        'icon_prefix'         => 'Prefijo del ícono',
        'icons'               => 'Iconos',
        'icon'                => 'Icono',
        'count_icons'         => 'Iconos',
        'extra_css'           => 'Hoja de estilos extra',
        'category'            => 'Categoría',
        'feature'             => 'Atributo',
        'in_search'           => 'Mostrar en el buscador (frontend)',
        'is_hidden'           => 'Oculto',
        'has_value'           => 'Valor obligatorio',
        'has_no_value'        => 'Valor opcional',
        'package'             => 'Paquete',
        'sort_order'          => 'Orden',
        'overview'            => 'General',
        'info'                => 'Información',
        'info_comment'        => 'Información extra, que podrá ser sobre escrita al asociar un atributo',
        'url_comment'         => 'Escriba una url externa de su paquete. Es necesario que sea un CDN seguro, como https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css',
        'package_comment'     => 'Suba el paquete de archivos css y fuentes, en formato zip',
        'library_comment'     => 'Nombre su paquete de íconos',
        'icon_prefix_comment' => 'Algunos paquetes de íconos necesitan una clase css como prefijo, como es el caso de FontAwesome que utiliza "fa"',
        'extra_css_comment'   => 'Escriba cualquier estilo css extra',
        'create_method'       => 'Seleccione como crear la librería de íconos',
        'create_by_url'       => 'Crear mediante url',
        'create_by_package'   => 'Crear mediante la subida de paquete en formato zip',
    ],
    'formwidgets' => [
        'iconselector' => [
            'name' => 'Selector de icono',
        ],
    ],
    'tab'         => [
        'library'    => 'Librería',
        'icons'      => 'Iconos',
        'view_icons' => 'Ver íconos',
        'editor'     => 'Editor',
    ],
    'permission'  => [
        'access_features'   => 'Administrar atributos',
        'access_categories' => 'Administrar categorías',
        'access_icons'      => 'Administrar íconos',
    ],

    'common' => [
        'error_generating_file' => 'Error al crear el archivo :path'
    ]
];