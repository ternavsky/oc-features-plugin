<?php
/**
 * Copyright (c) 2016 Planeta del Este .
 *
 * add_info_features_table.php is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

/**
 * Created by alvaro.
 * User: alvaro
 * Date: 15/12/15
 * Time: 07:47 AM
 */
namespace PlanetaDelEste\Features\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

/**
 * Class add_info_features_table
 * @package PlanetaDelEste\Features\Updates
 */
class AddInfoFeaturesTable extends Migration
{

    public function up()
    {
        Schema::table('planetadeleste_features_features', function ($table) {
        	/**
             * @var $table \Illuminate\Database\Schema\Blueprint
             */
        	$table->text('info')->nullable();
        });
    }

    public function down()
    {
        Schema::table('planetadeleste_features_features', function ($table) {
            /**
             * @var $table \Illuminate\Database\Schema\Blueprint
             */
            $table->dropColumn('info');
        });
    }

}