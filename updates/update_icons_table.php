<?php
/**
 * Copyright (c) 2016 Planeta del Este .
 *
 * update_icons_table.php is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PlanetaDelEste\Features\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateIconsTable extends Migration
{

    public function up()
    {
        Schema::table('planetadeleste_features_icons', function($table)
        {
            /**
             * @var $table \Illuminate\Database\Schema\Blueprint
             */
            $table->text('stylesheet')->nullable();
        });
    }

    public function down()
    {
        Schema::table('planetadeleste_features_icons', function($table)
        {
            /**
             * @var $table \Illuminate\Database\Schema\Blueprint
             */
            $table->dropColumn('stylesheet');
        });
    }

}