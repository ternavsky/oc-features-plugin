<?php
/**
 * Copyright (c) 2016 Planeta del Este .
 *
 * create_features_table.php is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PlanetaDelEste\Features\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateFeaturesTable extends Migration
{

    public function up()
    {
        Schema::create('planetadeleste_features_features', function ($table)
        {
            /**
             * @var $table \Illuminate\Database\Schema\Blueprint
             */
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->timestamps();
            $table->integer('category_id')->unsigned()->index();
            $table->string('name', 100);
            $table->string('icon', 64)->nullable();
            $table->boolean('is_hidden')->default(false);
            $table->boolean('has_value')->default(true);
            $table->boolean('in_search')->default(true);

            $table->foreign('category_id')->references('id')->on('planetadeleste_features_categories')->onDelete('cascade');
        });
    }

    public function down()
    {
        Schema::dropIfExists('planetadeleste_features_features');
    }

}
