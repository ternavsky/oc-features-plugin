<?php
/**
 * Copyright (c) 2016 Planeta del Este .
 *
 * Plugin.php is part of PlanetaDelEste.Features.
 *
 *     PlanetaDelEste.Features is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     PlanetaDelEste.Features is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with PlanetaDelEste.Features.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PlanetaDelEste\Features;

use App;
use Backend;
use Illuminate\Foundation\AliasLoader;
use Lang;
use System\Classes\PluginBase;

/**
 * Features Plugin Information File
 */
class Plugin extends PluginBase
{

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => Lang::get('planetadeleste.features::lang.app.name'),
            'description' => Lang::get('planetadeleste.features::lang.app.description'),
            'author'      => 'PlanetaDelEste',
            'icon'        => 'icon-th'
        ];
    }

    public function registerNavigation()
    {
        return [
            'features' => [
                'label' => Lang::get('planetadeleste.features::lang.features.menu_label'),
                'url' => Backend::url('planetadeleste/features/features'),
                'icon' => 'icon-th',
                'permissions' => ['planetadeleste.features.*'],
                'order' => 100,
                'sideMenu' => [
                    'features' => [
                        'label' => Lang::get('planetadeleste.features::lang.features.menu_label'),
                        'icon' => 'icon-th',
                        'url' => Backend::url('planetadeleste/features/features'),
                        'permissions' => ['planetadeleste.features.access_features']
                    ],
                    'categories' => [
                        'label' => Lang::get('planetadeleste.features::lang.categories.menu_label'),
                        'icon' => 'icon-magic',
                        'url' => Backend::url('planetadeleste/features/categories'),
                        'permissions' => ['planetadeleste.features.access_categories']
                    ],
                    'icons' => [
                        'label' => Lang::get('planetadeleste.features::lang.icons.menu_label'),
                        'icon' => 'icon-flag',
                        'url' => Backend::url('planetadeleste/features/icons'),
                        'permissions' => ['planetadeleste.features.access_icons']
                    ],
                ]
            ]
        ];
    }

    public function registerFormWidgets()
    {
        return [
            'PlanetaDelEste\Features\FormWidgets\IconSelector' => [
                'label' => 'planetadeleste.features::lang.formwidgets.iconselector.name',
                'code'  => 'iconselector'
            ]
        ];
    }

    public function registerPermissions()
    {
        return [
            'planetadeleste.features.access_features'  => ['tab' => Lang::get('planetadeleste.features::lang.app.name'), 'label' => Lang::get('planetadeleste.features::lang.permission.access_features')],
            'planetadeleste.features.access_categories'  => ['tab' => Lang::get('planetadeleste.features::lang.app.name'), 'label' => Lang::get('planetadeleste.features::lang.permission.access_categories')],
            'planetadeleste.features.access_icons'  => ['tab' => Lang::get('planetadeleste.features::lang.app.name'), 'label' => Lang::get('planetadeleste.features::lang.permission.access_icons')],
        ];
    }

    public function boot()
    {
        // Service provider
        App::register('\Chumper\Zipper\ZipperServiceProvider');

        // Register alias
        $alias = AliasLoader::getInstance();
        $alias->alias('Zipper', '\Chumper\Zipper\Zipper');

    }

}
